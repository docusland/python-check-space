#!/usr/bin/env python3

import psutil
import shutil
import smtplib, ssl
from dotenv import load_dotenv
import os
import heapq
"""
This script is meant to check the space availability on a given server. 
Some constants are defined in order to specify the rules.
Checks are applied on all drives available on computer.

An alert arises if one of the following rules is not respected
 - If more than MAXIMUM_PERCENTAGE_USED of the folder is used
 - If less than MINIMUM_SPACE_REQUIRED in GB is available
 
 If an alert arises, a list of : 
  - the 5 biggest files present on the corresponding drive : name and size
  - the 5 latest files present on the corresponding drive : name and size
"""



# Constants
ALERT_EMAIL= 'email'
ALERT_SMS = 'sms'

BYTE_TO_GIGABYTE = 1024 * 1024 * 1024

def send_mail(message: str):
    """ Send email to EMAIL_TO_ADRESS """
    port = 465
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(os.environ.get('EMAIL_FROM_ADRESS'), os.environ.get('EMAIL_FROM_PASSWORD'))
        server.sendmail(os.environ.get('EMAIL_FROM_ADRESS'), os.environ.get('EMAIL_TO_ADRESS'), message)

def compile_error_and_send_notification():
    """
        Compile errors in error variable.
        Add additional info latest and largest fils from drives.
    """
    if len(errors) > 0 :
        if ALERT_EMAIL == os.environ.get('ALERT_TYPE'):
            # Adding headers
            message = "Content-type: text/html\r\nSubject: " + os.environ.get('EMAIL_NAME') + "\r\n\r\n"

            # Compile errors
            message += '\r\n'.join(errors)

            ## Add additionnal info
            for drive in drives_to_parse:
                iterable = [os.path.join(dp, f) for dp, dn, fn in os.walk(os.path.expanduser(drive)) for f in fn]
                try:
                    largest_file = heapq.nlargest(5,iterable,key=os.path.getsize)
                    latest_file = heapq.nlargest(5,iterable,key=os.path.getctime)
                    message += '\r\n' + drive + ':'
                    message += '\r\nLargest Files :'
                    for f in largest_file:
                        message += '\r\n\t-'+f
                    message += '\r\nLatest Files'
                    for f in latest_file:
                        message += '\r\n\t-' + f
                except Exception as e:
                    message += '\r\n\t- SKIPPED ' + e
            
            send_mail(message)
    else:
        print('All is good')

def check_partition(partion) -> tuple:
    """
    Check if partition respects your requirements on MAXIMUM_PERCENTAGE_USED or MINIMUM_SPACE_REQUIRED
    :param partion: (device, mountpoint, fstype, opts) tuple
    :return: None
    """
    global errors
    global drives_to_parse

    drive = partition.mountpoint
    path = partition.device
    
    disk_usage = shutil.disk_usage(path)
    percentage = disk_usage.used / disk_usage.total

    # Check percentage use
    if percentage * 100 > int(os.environ.get('MAXIMUM_PERCENTAGE_USED')):
        errors.append('%s : Not enough space left, %2.2f percent used' % (drive, percentage))
        drives_to_parse.append(drive)
    else:
        print('%s : You have enough space left, %2.2f percent used' % (drive, percentage))

    # Check minimum space
    free_space = disk_usage.free / BYTE_TO_GIGABYTE
    if free_space < int(os.environ.get('MINIMUM_SPACE_REQUIRED')):
        errors.append('%s : Not enough space left, %2.2f GB left' % (drive, free_space))
        drives_to_parse.append(drive)
    else:
        print('%s : Everything is fine, we have %2.2f GB left' % (drive, free_space))

### Main script

errors = [] # automatically dealt by script
drives_to_parse = [] # automatically dealt by script


if __name__ == "__main__":
    load_dotenv() # Load .env file
    
    # Parse all drives.
    partitions = psutil.disk_partitions()
    for partition in partitions:
        check_partition(partition)
    compile_error_and_send_notification()
